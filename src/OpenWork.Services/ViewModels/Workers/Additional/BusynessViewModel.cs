﻿using System;

using OpenWork.Domain.Common;
using OpenWork.Domain.Entities;

namespace OpenWork.Services.ViewModels.Workers.Additional;

public class BusynessViewModel : BaseEntity
{
	public long WorkerId { get; set; }
	public DateTime Start { get; set; }
	public DateTime End { get; set; }
	public static implicit operator BusynessViewModel(Busyness entity)
	{
		return new BusynessViewModel
		{
			WorkerId = entity.WorkerId,
			Start = entity.Start,
			End = entity.End,
			Id = entity.Id
		};
	}
}
