﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

using Microsoft.IdentityModel.Tokens;

using OpenWork.DataAccess.Interfaces;
using OpenWork.Domain.Entities;
using OpenWork.Services.Common.Exceptions;
using OpenWork.Services.Common.Utils;
using OpenWork.Services.Dtos.Workers;
using OpenWork.Services.Interfaces;
using OpenWork.Services.Interfaces.Common;
using OpenWork.Services.Interfaces.Security;
using OpenWork.Services.ViewModels.Workers;
namespace OpenWork.Services.Services;

public class WorkerService : IWorkerService
{
	private readonly IFileService _filer;
	private readonly IUnitOfWork _repository;
	private readonly IHasher _hasher;
	private readonly IAuthManager _auth;
	private readonly IIdentityService _identity;
	private readonly IPaginatorService _paginator;
	private readonly IEmailService _email;
	private int _pageSize = 20;

	public WorkerService(IUnitOfWork repository, IHasher hasher, IAuthManager auth, IIdentityService identity, IPaginatorService paginator, IFileService filer, IEmailService email)
	{
		_filer = filer;
		_repository = repository;
		_hasher = hasher;
		_auth = auth;
		_identity = identity;
		_paginator = paginator;
		_email = email;
	}

	public async Task<bool> DeleteAsync()
	{
		_ = await _repository.Workers.DeleteAsync(_identity.Id);
		return await _repository.SaveChangesAsync() > 0;
	}

	public async Task<IEnumerable<WorkerBaseViewModel>> SearchAsync(SearchDto dto, int page)
	{
		var query =
				_repository.Workers.GetAll()
				.Where(
					wrk => wrk.Skills.Any(
						skl => dto.AllowedSkillsId.Any(id => id == skl.Id)
					)
				)
				;
		switch(dto.SortOptions)
		{
			case SortOptions.AscendingOnline:
				query = query.OrderBy(x => x.LastSeen);
				break;
			case SortOptions.DescendingOnline:
				query = query.OrderByDescending(x => x.LastSeen);
				break;
			case SortOptions.AscendingExperience:
				query = query.OrderBy(x => x.Comments.Count());
				break;
			case SortOptions.DescendingExperience:
				query = query.OrderByDescending(x => x.Comments.Count());
				break;
			case SortOptions.AscendingRating:
				query = query.OrderBy(x => x.Comments.IsNullOrEmpty() ? 0 : x.Comments.Average(x => x.Satisfied ? 1 : (double)0) * 100);
				break;
			case SortOptions.DescendingRating:
				query = query.OrderByDescending(x => x.Comments.IsNullOrEmpty() ? 0 : x.Comments.Average(x => x.Satisfied ? 1 : (double)0) * 100);
				break;
		}
		return (await _paginator.PaginateAsync(query, new PaginationParams(_pageSize, page))).Select(
				wrk => new WorkerBaseViewModel
				{
					Id = wrk.Id,
					Surname = wrk.Surname,
					LastSeen = wrk.LastSeen,
					Name = wrk.Name,
					Phone = wrk.Phone,
					Image = wrk.Image,
					Rating = wrk.Comments.IsNullOrEmpty() ? null : wrk.Comments.Average(x => x.Satisfied ? 1 : 0) * 100
				}
			);
		;
	}

	public async Task<bool> OnlineAsync()
	{
		Worker entity = await _repository.Workers.GetAsync(_identity.Id);
		entity.LastSeen = DateTime.Now;
		_repository.Workers.Update(entity);
		return await _repository.SaveChangesAsync() > 0;
	}

	public async Task<WorkerBaseViewModel> GetBaseAsync()
	{
		return await GetBaseAsync(_identity.Id);
	}

	public async Task<WorkerViewModel> GetAsync()
	{
		return await GetAsync(_identity.Id);
	}

	public async Task<WorkerViewModel> GetAsync(long id)
	{
		Worker entity = await _repository.Workers.GetAsync(id);
		if(entity is null)
			throw new StatusCodeException(System.Net.HttpStatusCode.NotFound, "Worker not found");
		var res = new WorkerViewModel
		{
			Skills = entity.Skills.Select(x => new SkillViewModel
			{
				CategoryId = x.CategoryId,
				Description = x.Description,
				Id = x.Id,
				Name = x.Name,
			}).ToList(),
			Image=entity.Image,
			Surname = entity.Surname,
			LastSeen = entity.LastSeen,
			Email = entity.Email,
			Id = entity.Id,
			Name = entity.Name,
			Phone = entity.Phone,
			Rating = entity.Comments.IsNullOrEmpty() ? null : entity.Comments.Average(x => x.Satisfied ? 1 : (double)0) * 100
		};
		return res;
	}

	public async Task<WorkerBaseViewModel> GetBaseAsync(long id)
	{
		Worker entity = await _repository.Workers.GetAsync(id);
		return new WorkerBaseViewModel
		{
			Id = entity.Id,
			Surname = entity.Surname,
			LastSeen = entity.LastSeen,
			Name = entity.Name,
			Image = entity.Image,
			Phone = entity.Phone,
			Rating = entity.Comments.IsNullOrEmpty() ? null : entity.Comments.Average(x => x.Satisfied ? 1 : 0) * 100
		};
	}

	public async Task<string> LoginAsync(WorkerLoginDto dto)
	{
		Worker entity = await _repository.Workers.GetAsync(dto.Email);
		if(entity is not null)
			if(entity.Banned)
				throw new StatusCodeException(System.Net.HttpStatusCode.Forbidden, "You are banned");
			else if(_hasher.Verify(entity.Password, dto.Password, entity.Email))
				return _auth.GenerateToken(entity);
			else
				throw new StatusCodeException(System.Net.HttpStatusCode.BadRequest, "Invalid password");
		else
			throw new StatusCodeException(System.Net.HttpStatusCode.NotFound, "Worker not found");
	}

	public async Task<bool> RegisterAsync(WorkerRegisterDto dto)
	{
		Worker result = await _repository.Workers.GetAsync(dto.Email);
		if(result is not null)
			if(result.Banned)
				throw new StatusCodeException(System.Net.HttpStatusCode.Forbidden, "You are banned");
			else
				throw new StatusCodeException(System.Net.HttpStatusCode.BadRequest, "Worker with this email already exists");
		Worker entity = dto;
		entity.Password = _hasher.Hash(dto.Password, dto.Email);
		if(dto.Image is not null)
			entity.Image = await _filer.SaveImageAsync(dto.Image);
		_ = _repository.Workers.Add(entity);
		return await _repository.SaveChangesAsync() > 0;
	}

	public async Task<bool> UpdateAsync(WorkerRegisterDto dto)
	{
		Worker entity = await _repository.Workers.GetAsync(_identity.Id);
		if(dto.Image is not null)
			entity.Image = await _filer.SaveImageAsync(dto.Image);
		entity.PhoneVerified = entity.Phone == dto.Phone;
		entity.Phone = dto.Phone;
		entity.EmailVerified = entity.Email == dto.Email;
		entity.LastSeen = DateTime.Now;
		entity.Email = dto.Email;
		entity.Name = dto.Name;
		entity.Surname = dto.Surname;
		entity.Password = _hasher.Hash(dto.Password, dto.Email);
		_ = _repository.Workers.Update(entity);
		return await _repository.SaveChangesAsync() > 0;
	}

	public async Task<IEnumerable<WorkerBaseViewModel>> GetAllAsync(int page)
	{
		return (await
			_paginator.PaginateAsync(
				_repository.Workers.GetAll()
			, new PaginationParams(_pageSize, page))).Select(
				wrk => new WorkerBaseViewModel
				{
					Id = wrk.Id,
					Surname = wrk.Surname,
					LastSeen = wrk.LastSeen,
					Name = wrk.Name,
					Rating = wrk.Comments.IsNullOrEmpty() ? null : wrk.Comments.Average(x => x.Satisfied ? 1 : 0) * 100
				}
			);
	}

	public async Task<bool> ForgotPasswordAsync(string email)
	{
		Worker worker = await _repository.Workers.GetAsync(email);
		if(worker is null)
			throw new StatusCodeException(System.Net.HttpStatusCode.NotFound, "Worker with this email not found");
		if(worker.Banned)
			throw new StatusCodeException(System.Net.HttpStatusCode.Forbidden, "You are banned");
		string newPassword = Guid.NewGuid().ToString();
		worker.Password = _hasher.Hash(newPassword, worker.Email);
		await _email.SendMailAsync(worker.Email, $"Your new password for Open Work is \"{newPassword}\" without quotes. You can change it simply by updating your account info after logging in with this password.");
		return await _repository.SaveChangesAsync() > 0;
	}
}
