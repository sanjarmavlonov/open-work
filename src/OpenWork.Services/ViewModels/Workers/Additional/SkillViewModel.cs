﻿using OpenWork.Domain.Common;
using OpenWork.Domain.Entities;

namespace OpenWork.Services.ViewModels.Workers;

public class SkillViewModel : BaseEntity
{
	public string Name { get; set; } = string.Empty;
	public string Description { get; set; } = string.Empty;
	public long CategoryId { get; set; }
	public static implicit operator SkillViewModel(Skill entity)
	{
		return new Skill()
		{
			Id = entity.Id,
			Name = entity.Name,
			Description = entity.Description,
			CategoryId = entity.CategoryId,
		};
	}
}
