﻿using System.Collections.Generic;
using System.Linq;

using OpenWork.Domain.Common;
using OpenWork.Domain.Entities;
using OpenWork.Services.ViewModels.Workers;

namespace OpenWork.Services.ViewModels.Admins;

public class CategoryViewModel : BaseEntity
{
	public string Name { get; set; } = string.Empty;

	public List<SkillViewModel> Skills { get; set; } = new List<SkillViewModel>();

	public static implicit operator CategoryViewModel(Category entity)
	{
		return new CategoryViewModel()
		{
			Id = entity.Id,
			Skills = entity.Skills.Select(x => (SkillViewModel)x).ToList(),
			Name = entity.Name,
		};
	}
}
