﻿using System.Collections.Generic;

using OpenWork.Domain.Entities;

namespace OpenWork.Services.ViewModels.Workers;

public class WorkerViewModel : WorkerBaseViewModel
{
	public string Email { get; set; } = string.Empty;

	public IList<SkillViewModel> Skills { get; set; } = default!;

	public static implicit operator WorkerViewModel(Worker entity)
	{
		return new WorkerViewModel()
		{
			Id = entity.Id,
			Name = entity.Name,
			Email = entity.Email,
			Phone = entity.Phone,
			Surname = entity.Surname,
			LastSeen = entity.LastSeen,
			Image = entity.Image,
		};
	}
}
