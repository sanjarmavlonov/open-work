﻿using System.Security.Claims;

using Microsoft.AspNetCore.Http;

using OpenWork.Services.Common.Exceptions;
using OpenWork.Services.Interfaces.Common;

namespace OpenWork.Services.Services.Common;

public class IdentityService : IIdentityService
{
	private readonly IHttpContextAccessor _accessor;

	public IdentityService(IHttpContextAccessor accessor)
	{
		_accessor = accessor;
	}
	public long Id
	{
		get
		{
			Claim? result = _accessor.HttpContext.User.FindFirst("Id");
			return result is not null ? long.Parse(result.Value) : throw new StatusCodeException(System.Net.HttpStatusCode.Unauthorized, "You have to authorize to use this features");
		}
	}
	public string Email
	{
		get
		{
			Claim? result = _accessor.HttpContext!.User.FindFirst("Email");
			return result is null ? throw new StatusCodeException(System.Net.HttpStatusCode.Unauthorized, "You have to authorize to use this features") : result.Value;
		}
	}

	public string Role
	{
		get
		{
			Claim? result = _accessor.HttpContext!.User.FindFirst(ClaimTypes.Role);
			return result is null ? throw new StatusCodeException(System.Net.HttpStatusCode.Unauthorized, "You have to authorize to use this features") : result.Value;
		}
	}
}