﻿using System.Collections.Generic;
using System.Threading.Tasks;

using OpenWork.Domain.Entities;
using OpenWork.Services.Dtos.Workers;
using OpenWork.Services.ViewModels.Workers.Additional;

namespace OpenWork.Services.Interfaces;

public interface IBusynessService
{
	public Task<bool> CreateAsync(BusynessCreateDto dto);
	public Task<bool> DeleteAsync(long id);
	public Task<IEnumerable<BusynessViewModel>> GetAllAsync(long workerId, int page);
	public Task<IEnumerable<BusynessViewModel>> SearchAsync(BusynessSearchDto dto, int page);

	public Task<IEnumerable<BusynessViewModel>> GetAllAsync(int page);
}
